#example 1
x=10
y=10
print(id(x))
print(id(y))

print( x is y)  # true
print( x is not y) # false

#example 2
x=10
y=20
print(id(x))
print(id(y))

print(x is y)  #false
print( x is not y) #true
