x=2
y=5
print(x+y)   # 7
print(x-y)   # -3
print(x*y)   # 10
print(x/y)   # 0.4(float)
print(x%y)   # 2
print(x//y)   # 0
print(x**y)   # 32 (power 2^5=32)
