x = True
y = False
print(x and y)    # false
print(x or y)    # true
print( not x)    # false

a=10
b=20
print(a and b)  #20
print(a or b)  #10
print(not a)  #false

a=0
b=20
print(a and b)  #0
print(a or b)  #20
print(not a)  #true
