''' print following pattern
    1 2 3 
    4 5 6
    7 8 9
'''

row = int(input("Enter the number of rows: "))

num=1
for i in range (row):
    for j in range(row):
        print(num,end=" ")
        num=num+1
    print()
