'''
WAP to print all the character values of the given ASCII value range from the user
Input :
    Enter the start of range : 1
    Enter end of range: -2
    Output :
    Wrong input
    Input :
    Enter start of range : 65
    Enter end of range : 67
    Output :
    The character of ASCII value 65 is A.
    The character of ASCII value 66 is B.
    The character of ASCII value 67 is C.
    .
    .
    The character of ASCII value 89 is Y.
'''

'''
start = int(input("Enter the Starting number: "))
if(start<0):
    raise Exception("Number should be positive")

end=int(input("Enter the Ending Number: "))
if(start<0):
    raise Exception("Number should be positive")

for i in range(start,end+1):
    print("The character of ASCII value", i," is: ",chr(i))
print()
'''

def start ():
    startnum = int(input("Enter the Starting Number: "))
    if(startnum<0):
        print("Number must be positive")
        start()
    return startnum

def end ():
    endnum = int(input("Enter the Starting Number: "))
    if(endnum<0):
        print("Number must be positive")
        end()
    return endnum

startnum=start()
endnum=end()
for i in range(startnum,endnum+1):
    print("The character of ASCII value", i,"is: ", chr(i))
print()
