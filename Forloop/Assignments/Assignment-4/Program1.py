'''
Program 1:
    WAP to print numbers from a given range.
    Input :
    Start = 100;
    End = 110;
    Output:
    100 101 103 104 105 106 107 108 109
'''

start = int(input("Enter the Starting Number: "))

end= int(input("Enter the ending Number: "))

for i in range(start,end+1):
    print(i,end=" ")
print()
