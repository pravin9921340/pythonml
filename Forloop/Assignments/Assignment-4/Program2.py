'''
Write a Program to print all Even numbers from a given range.
Input :
    Start = 10;
    End = 20;
    Output:
    10 12 14 16 18
'''
start = int(input("Enter starting Number: "))

end = int(input("Enter the Ending Number: "))

for i in range(start,end+1):
    if i%2==0 :
        print(i,end=" ")
print()
