'''
WAP to print all the ASCII values from a given character range.
Input :
    Enter the start of range : A
    Enter end of range: Z
    Output :
    ASCII value of A is 65
    ASCII value of B is 66
    ASCII value of C is 67
    .
    .
    .
    ASCII value of Y is 89
'''

start = input("Enter the Starting Character of range: ")

end = input("Enter the Ending Character of range: ")

for i in range(ord(start),ord(end)+1):
    print(i,end=" ")

