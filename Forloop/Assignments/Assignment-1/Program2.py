'''
Program 2: Write a Program to check whether the number is Negative,
Positive or equal to Zero.
Input: -2
Output: -2 is the negative number
'''

n = int(input("Enter the number:"))

if n>0 :
    print(n,"is the Positive number")
elif n<0 :
    print(n,"is the Negative Number")
else :
    print(n,"is equal 0" )
