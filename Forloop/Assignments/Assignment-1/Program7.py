'''
Program 7: Write a Program to take a number of months and print the number of
days in that month.
Input: 4
Output: April is a 30-day month.
'''
month = int(input("Enter the number of the month (1-12): "))

# Define a list to store the number of days in each month
days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

# Check if the input is within a valid range (1-12)
if 1 <= month <= 12:
    month_names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    month_name = month_names[month - 1]
    num_days = days_in_month[month - 1]
    print(month_name," is a", num_days,"-day month.")
else:
    print("Invalid input. Please enter a number between 1 and 12.")

