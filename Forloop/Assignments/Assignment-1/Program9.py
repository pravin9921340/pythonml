''' Program 9: Write a program to check whether the input character is a vowel or
consonant
Input: ‘a’
Output: vowel
Input: ‘b’
Output: consonant
'''
char = input("Enter a character: ")

# Convert the input character to lowercase to handle both uppercase and lowercase characters
char = char.lower()

# Check if the input is a single alphabet character
if len(char) == 1 and char.isalpha():
    if char in "aeiou":
        print("vowel")
    else:
        print("consonant")
else:
    print("Invalid input. Please enter a single alphabet character.")

