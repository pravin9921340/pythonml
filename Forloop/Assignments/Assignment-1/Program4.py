'''
Program 4: Write a Program to check whether the number is divisible by 5
or not.
Input: 55
Output: 55 is divisible by 5
'''
n = int(input("Enter the Number: "))

if n%5==0 :
    print(n,"is Divisible by 5")
else :
    print(n," is not Divisible by 5 ")

