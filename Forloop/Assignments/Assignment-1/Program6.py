'''
Program 6: Write a Program to check whether the Character is Alphabet or
not.
Input: v
Output: v is an alphabet.
'''
input_char = input("Enter a character: ")


if input_char.isalpha():
    print(input_char + " is an alphabet.")
else:
    print(input_char + " is not an alphabet.")

