'''
Program 5: Write a Program to take an integer ranging from 0 to 6 and print
corresponding weekday (week starting from Monday)
Input: 2
Output: Wednesday.
'''
n = int(input("Enter the value ranging from 0 to 6: "))

if n==0:
    print("Monday")
elif n==1:
    print("Tuesday")
elif n==2:
    print("Wednesday")
elif n==3:
    print("Thursday")
elif n==4:
    print("Friday")
elif n==5:
    print("Saturday")
elif n==6:
    print("Sunday")
else:
    print("Invalid Input")
