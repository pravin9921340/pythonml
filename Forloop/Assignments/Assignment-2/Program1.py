'''
1. WAP to check numbers are divisible by 4 and 5 Print those numbers
Input1: num1 =20
Output:20 is divisible by 4 and 5
Input1: num1 =15
Output:15 is not divisible by 4 and 5
'''

num = int(input("Enter the Number: "))

if num%4==0 and num%5==0:
    print(num, "is divisible by 4 and 5")
else:
    print(num ,"is not divisible by 4 and 5 ")
