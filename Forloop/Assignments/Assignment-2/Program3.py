'''
Take two numbers from users and print the sum of those numbers
if the sum is even.
Input1: num1 = 10
Input2: num2 = 20
Output: 30 is Even

Input1: num1 = 10
Input2: num2 = 15
Output: No Output
'''

num1= int(input("Enter the Number: "))

num2= int(input("Enter the second number: "))
sum = num1+num2

if sum%2==0:
    print(sum," is Even")
else:
    print(sum," is Odd ")

