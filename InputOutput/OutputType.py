x=10
y=20

print(x)     #10
print()      
print(y)     #20

#Type2 
#print(string)

print('core2web')     #core2web
print("core2web")     #core2web
print('''core2web''') #core2web

print('core2web' + 'Incubator')      #core2webIncubator
print('core2web' , 'Incubator')      #core2web Incubator

#type3 
#print( argument list)

x=10
y=20

print(x,y)  # 10 20
print(x,y, sep =':')   # 10 : 20
print(x,y, sep =',')   # 10 , 20
print(x,y, sep = '===') # 10 ===20

# end 

print(x)  #10
print(y)  #20

print(x,end = " ")  # 10 20
print(y)


#type4
#print (formated output)

x=10
y=20.5

print("value of x = %i" %x)   # value of x=10
print("value of x = %d" %x)   # value of x=10
print("value of x = %f" %y)   # value of y=20.5

print("value of x={} and value of y={}".format(x,y))  # value of x= 10 and value of y= 20.5


